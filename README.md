# java-backend-test project

This project use for CT Corp Java Backend Assingment  

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

## For Setup Application.properties
```shell script
quarkus.datasource.db-kind=postgresql
quarkus.datasource.jdbc.url=jdbc:postgresql://localhost:5432/ctcorp
quarkus.datasource.username=<your setting>
quarkus.datasource.password=<your setting>
quarkus.datasource.jdbc.max-size=16
quarkus.datasource.jdbc.min-size=2
quarkus.hibernate-orm.database.generation=drop-and-create
```
## CURL Post
```shell script
curl --location --request GET 'localhost:8080/post' 
```

## CURL Tags
```shell script
curl --location --request GET 'localhost:8080/tags' 
```
