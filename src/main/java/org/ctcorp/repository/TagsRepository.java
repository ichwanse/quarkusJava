package org.ctcorp.repository;

import org.ctcorp.model.Tags;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagsRepository extends JpaRepository<Tags,Long> {
}
