package org.ctcorp.resource;

import org.ctcorp.model.Tags;
import org.ctcorp.repository.TagsRepository;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Path("/tags")
public class TagsResource {
    private final TagsRepository tagsRepository;

    public TagsResource(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTags(){
        List<Tags> tags = tagsRepository.findAll();
        return Response.ok(tags).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long id){
        return tagsRepository.findById(id)
                .map(tag -> Response.ok(tag).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Tags tags){
        tagsRepository.save(tags);
        return Response.created(URI.create("/tags/" + tags.getId())).build();
    }

    @PUT
    @Path("{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Tags tags, @PathParam("id") Long id){
        Optional<Tags> getTag = tagsRepository.findById(id);
        if(getTag.isPresent()){
            Tags tag = getTag.get();
            tag.setLable(tags.getLable());
            tagsRepository.save(tag);

            return Response.created(URI.create("/tags/" + tag.getId())).build();
        }
        throw new IllegalArgumentException("No tag id " + id + " exists");
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteBydId(@PathParam("id") Long id) {
        Optional<Tags> getTag = tagsRepository.findById(id);
        if(getTag.isPresent()) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
