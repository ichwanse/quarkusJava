package org.ctcorp.resource;

import org.ctcorp.model.Post;
import org.ctcorp.model.Tags;
import org.ctcorp.repository.PostRepository;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Path("/post")
public class PostResource {
    private final PostRepository postRepository;

    public PostResource(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPost(){
        List<Post> post = postRepository.findAll();
        return Response.ok(post).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long id){
        return postRepository.findById(id)
                .map(post -> Response.ok(post).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Post post){
        postRepository.save(post);
        return Response.created(URI.create("/post/" + post.getId())).build();
    }

    @PUT
    @Path("{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Post post, @PathParam("id") Long id){
        Optional<Post> getPost = postRepository.findById(id);
        if(getPost.isPresent()){
            Post p= getPost.get();
            p.setTitle(post.getTitle());
            p.setContent(post.getContent());

            postRepository.save(p);

            return Response.created(URI.create("/post/" + p.getId())).build();
        }
        throw new IllegalArgumentException("No post id " + id + " exists");
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteBydId(@PathParam("id") Long id) {
        Optional<Post> getPost = postRepository.findById(id);
        if(getPost.isPresent()) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
