package org.ctcorp.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Tags")
@Table(name = "tags")
public class Tags {
    @Id
    @SequenceGenerator(
            name = "tag_sequence",
            sequenceName = "tag_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "tag_sequence"
    )
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "lable", nullable = false, columnDefinition = "TEXT")
    private String lable;

    @ManyToMany(mappedBy = "tags")
    private List<Post> post = new ArrayList<>();

    public Tags() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }
}
